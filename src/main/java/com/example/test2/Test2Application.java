package com.example.test2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class Test2Application {

	public static void main(String[] args) {

		//ConfigurableApplicationContext context=SpringApplication.run(Test2Application.class, args);
		ApplicationContext context= new ClassPathXmlApplicationContext("interest.xml");
		 Interest interest=context.getBean("interest",Interest.class);
		 interest.CalcInt();
	}

}
