package com.example.test2;

import org.springframework.stereotype.Component;


public class PrivateBank {
    public int amt;
    public int time;

    public PrivateBank() {
        this.time=1;
        this.amt=100000;
    }

    public int getAmt() {
        return amt;
    }

    public void setAmt(int amt) {
        this.amt = amt;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

}
