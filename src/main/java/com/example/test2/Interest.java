package com.example.test2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public class Interest {
    public double interest;
 public int age;
    @Autowired
    PrivateBank PB;
    public Interest() {
        PB=new PrivateBank();
    }

    public Interest(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }
    public void CalcInt()
    {
        if(age>60)
            interest= (PB.amt * PB.time * 0.08)/100;
        else
            interest=(PB.amt* PB.time*0.04)/100;
        System.out.println(interest);
    }
}
